local config = require("config")

require("editor").init(config)
require("plugins").init(config)
require("lsp").init(config)
