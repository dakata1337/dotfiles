local config = {
    -- Basic editor behaviour
    editor = {
        map_leader = " ",
        tab_size = 4,
        focus_new_split = true,
        enable_truecolor = true,
        line_numbers = true,
        persist_undos = true,
        disable_arrows = true,
        use_system_clipboard = false,
        refresh_rate = 240,
        refresh_rate_idle = 60,
    },
    -- Plugins other than the integrated ones
    plugins = {
        ["dakata1337/mountaineer.vim"] = {
            lazy = false,
            priority = 1000,
            config = function()
                vim.cmd("colorscheme mountaineer-grey")
            end
        },
        ["Saecki/crates.nvim"] = {
            event = "VeryLazy",
            config = function()
                local crates = require("crates")
                crates.setup({
                    thousands_separator = ",",
                    popup = {
                        autofocus = true,
                        max_height = 10,
                        text = { pill_left = "█", pill_right = "█", }
                    },
                    highlight = {
                        loading = "Comment",
                        version = "Comment",
                        prerelease = "Comment",
                        yanked = "Comment",
                    },
                })

                local function crates_map(keymap, func, desc)
                    vim.keymap.set("n", keymap, function()
                        func()
                    end, { desc = desc })
                end

                crates_map("<leader>Cs", crates.show_popup, "Show crate info [Creates.io]")
                crates_map("<leader>Cf", crates.show_features_popup, "Show crate features [Creates.io]")
                crates_map("<leader>Cv", crates.show_versions_popup, "Show crate versions [Creates.io]")
                crates_map("<leader>Cd", crates.show_dependencies_popup, "Show crate dependencies [Creates.io]")
            end
        },
        ["simrat39/rust-tools.nvim"] = {
            lazy = true,
        },
    },
    lsp = {
        default_on_attach = function(client, bufnr)
            require("config").keymaps.lsp(client, bufnr)
        end,
        default_flags = {
            debounce_text_changes = 150,
        },
        servers = {
            -- Overrides the default server
            ["rust"] = {
                config = function(default_on_attach)
                    local rt = require("rust-tools")
                    rt.setup({
                        server = {
                            on_attach = default_on_attach
                        }
                    })
                    rt.inlay_hints.enable()
                end
            },
        }
    },
    -- Keymaps for the plugins
    keymaps = {
        harpoon = function()
            local wk = require("which-key")
            wk.register({ h = "harpoon" }, { prefix = "<leader>" })

            local telescope = require("telescope")
            local mark = require("harpoon.mark")
            local ui = require("harpoon.ui")

            vim.keymap.set("n", "<leader>ha", function()
                mark.add_file()
            end, { desc = "Mark the current file [Harpoon]" })

            vim.keymap.set("n", "<leader>ht", function()
                telescope.extensions.harpoon.marks()
            end, { desc = "Open marked files [Harpoon]" })

            vim.keymap.set("n", "<leader>hn", function()
                ui.nav_next()
            end, { desc = "Goto next mark [Harpoon]" })

            vim.keymap.set("n", "<leader>hp", function()
                ui.nav_prev()
            end, { desc = "Goto previous mark [Harpoon]" })
        end,
        telescope = function()
            local wk = require("which-key")
            wk.register({ f = "find" }, { prefix = "<leader>" })

            local builtin = require('telescope.builtin')
            vim.keymap.set('n', '<leader>ff', builtin.find_files, { desc = "Opens fuzzy file search" })
            vim.keymap.set('n', '<leader>fw', builtin.live_grep, { desc = "Opens fuzzy word search (inside files)" })
            vim.keymap.set('n', '<leader>fb', builtin.buffers, { desc = "Opens fuzzy buffer search" })
            vim.keymap.set('n', '<leader>fh', builtin.help_tags, { desc = "Opens fuzzy help search" })
        end,
        lsp = function(_, bufnr)
            local bufopts = function(desc)
                return { noremap = true, silent = true, buffer = bufnr, desc = desc }
            end
            vim.keymap.set('n', 'gD', vim.lsp.buf.declaration, bufopts("Goto Declaration [LSP]"))
            vim.keymap.set('n', 'gd', vim.lsp.buf.definition, bufopts("Goto Definition [LSP]"))
            vim.keymap.set('n', 'gr', vim.lsp.buf.references, bufopts("Goto References [LSP]"))
            vim.keymap.set('n', 'gi', vim.lsp.buf.implementation, bufopts("Goto Implementation [LSP]"))
            vim.keymap.set('n', 'gtD', vim.lsp.buf.type_definition, bufopts("Goto Type Definition [LSP]"))

            vim.keymap.set('n', 'K', vim.lsp.buf.hover, bufopts("Show Hover Documentation [LSP]"))

            local wk = require("which-key")
            wk.register({ l = "LSP" }, { prefix = "<leader>" })

            vim.keymap.set('n', '<leader>la', vim.lsp.buf.code_action, bufopts("Code Action [LSP]"))
            vim.keymap.set('n', '<leader>lf', function()
                vim.lsp.buf.format({ async = true })
            end, bufopts("Format Buffer [LSP]"))
            vim.keymap.set('n', '<leader>lr', vim.lsp.buf.rename, bufopts("Rename Symbol [LSP]"))
        end,
        comments = function()
            require("Comment").setup({
                padding = true,
                toggler = {
                    line = '<leader>c',
                    block = '<leader>/',
                },
                opleader = {
                    line = '<leader>c',
                    block = '<leader>/',
                },
            })
        end,
        todo_comments = function()
            local wk = require("which-key")
            wk.register({ t = { name = "todo comments" } }, { prefix = "<leader>" })

            vim.keymap.set("n", "<leader>tn", function()
                require("todo-comments").jump_next()
            end, { desc = "Next todo comment" })

            vim.keymap.set("n", "<leader>tp", function()
                require("todo-comments").jump_prev()
            end, { desc = "Previous todo comment" })
        end
    },
}
return config
