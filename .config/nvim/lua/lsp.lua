-- For additional servers check: https://github.com/neovim/nvim-lspconfig/blob/master/doc/server_configurations.md
local lsp_server_hashmap = {
    ["rust"]     = "rust_analyzer",
    ["csharp"]   = "omnisharp",
    ["markdown"] = "marksman",
    ["ts"]       = "tsserver",
    ["python"]   = "pyright",
    ["lua"]      = "lua_ls",
    ["c"]        = "clangd",
    ["go"]       = "gopls",
    ["zig"]      = "zls",
    ["vue"]      = "vuels",
    ["tailwind"] = "tailwindcss",
    ["emmet"]    = "emmet_ls",
    ["html"]     = "html",
    ["css"]      = "cssls",
    ["erlang"]   = "erlangls",
}

local function get_lsp_servers()
    local ret = {}
    for _, package in pairs(lsp_server_hashmap) do
        table.insert(ret, package)
    end
    return ret
end

local function setContains(set, key)
    return set[key] ~= nil
end

local function assert_config(config)
    if config.lsp.default_on_attach == nil then
        print("ERROR: `config.lsp.default_on_attach` doesn't exist!")
        return
    end
    if config.lsp.default_flags == nil then
        print("ERROR: `config.lsp.default_flags` doesn't exist!")
        return
    end
end

local M = {
    init = function(config)
        assert_config(config)

        local cmp = require("cmp")

        vim.api.nvim_set_option('updatetime', 250)
        vim.api.nvim_create_autocmd({ "CursorHold", "CursorHoldI" }, {
            pattern = { "*" },
            callback = function()
                if not cmp.visible() then
                    vim.diagnostic.open_float(nil, { focus = false })
                end
            end
        })

        require("mason").setup()
        require("mason-lspconfig").setup {
            ensure_installed = get_lsp_servers(),
        }

        -- Setup LSP
        local capabilities = require("cmp_nvim_lsp").default_capabilities()
        local lspconfig = require("lspconfig")

        for name, server_name in pairs(lsp_server_hashmap) do
            if setContains(config.lsp.servers, name) then
                config.lsp.servers[name].config(config.lsp.default_on_attach)
            else
                lspconfig[server_name].setup({
                    flags = config.lsp.default_flags,
                    on_attach = config.lsp.default_on_attach,
                    capabilities = capabilities,
                })
            end
        end

        -- Setup Auto Completion
        local luasnip = require("luasnip")

        -- stylua: ignore start
        local mappings = {
            ["<CR>"] = cmp.mapping.confirm({
                behavior = cmp.ConfirmBehavior.Replace,
                select = true,
            }),
            ["<Tab>"] = cmp.mapping(function(fallback)
                if cmp.visible() then
                    cmp.select_next_item()
                elseif luasnip.expand_or_jumpable() then
                    luasnip.expand_or_jump()
                else
                    fallback()
                end
            end, { 'i', 's' }),
            ["<S-Tab>"] = cmp.mapping(function(fallback)
                if cmp.visible() then
                    cmp.select_prev_item()
                elseif luasnip.jumpable(-1) then
                    luasnip.jump(-1)
                else
                    fallback()
                end
            end, { 'i', 's' }),
        }
        -- stylua: ignore end

        cmp.setup({
            snippet = {
                expand = function(args)
                    luasnip.lsp_expand(args.body)
                end,
            },
            mapping = cmp.mapping.preset.insert(mappings),
            sources = {
                { name = "nvim_lsp" },
                { name = "crates" },
                { name = "path",    max_item_count = 20 },
                { name = "luasnip" },
                { name = "buffer",  keyword_length = 5 },
                { name = "codeium" },
            }
        })
    end
}
return M
