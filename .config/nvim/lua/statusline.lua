local M = {
    config = function()
        local colors = {
            bg     = "#222222",
            fg     = "#f0f0f0",
            blue   = '#8F8AAC',
            cyan   = '#8AABAC',
            black  = '#080808',
            white  = '#c6c6c6',
            red    = '#AC8A8C',
            grey   = '#303030',
            yellow = '#ACA98A',
        }

        local theme = {
            normal = {
                a = { fg = colors.black, bg = colors.white },
                b = { fg = colors.white, bg = colors.grey },
                c = { fg = colors.fg, bg = colors.bg },
            },
            insert = { a = { fg = colors.black, bg = colors.blue } },
            visual = { a = { fg = colors.black, bg = colors.cyan } },
            replace = { a = { fg = colors.black, bg = colors.red } },
            command = { a = { fg = colors.black, bg = colors.yellow } },
            inactive = {
                a = { fg = colors.white, bg = colors.black },
                b = { fg = colors.white, bg = colors.black },
                c = { fg = colors.white, bg = colors.black },
            },
        }

        require("lualine").setup({
            options = {
                component_separators = { left = '', right = '' },
                section_separators = { left = '', right = '' },
                theme = theme,
            },
            sections = {
                lualine_a = { 'mode' },
                lualine_b = { 'branch', 'diff', 'diagnostics' },
                lualine_c = { 'filename' },
                lualine_x = { 'filetype' },
                lualine_y = { 'progress' },
                lualine_z = { 'location' }
            },
        })
    end
}
return M
