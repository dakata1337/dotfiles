local function or_default(val, def)
    if val == nil then
        return def
    end
    return val
end

local M = {
    init = function(config)
        local editor = config.editor

        vim.cmd "set noshowmode"
        vim.cmd "set laststatus=3"

        if editor.disable_arrows then
            for _, mode in pairs({ 'n', 'i', 'v', 'x' }) do
                for _, key in pairs({ '<Up>', '<Down>', '<Left>', '<Right>' }) do
                    vim.keymap.set(mode, key, '<nop>')
                end
            end
        end

        editor.focus_new_split = or_default(editor.focus_new_split, true)
        if editor.focus_new_split then
            vim.cmd "set splitbelow"
            vim.cmd "set splitright"
        end

        -- Set map leader
        editor.map_leader = or_default(editor.map_leader, " ")
        vim.g.mapleader = editor.map_leader

        -- Enable terminal true colors
        editor.enable_truecolor = or_default(editor.enable_truecolor, true)
        if editor.enable_truecolor then
            vim.cmd("set termguicolors")
        end

        -- Default tab size = 4
        editor.tab_size = or_default(editor.tab_size, 4)

        vim.opt.shiftwidth = editor.tab_size
        vim.opt.tabstop = editor.tab_size
        vim.opt.smarttab = true
        vim.opt.expandtab = true

        -- Editor line numbers
        editor.line_numbers = or_default(editor.tab_size, true)
        if editor.line_numbers then
            vim.wo.number = true
            vim.wo.relativenumber = true
        end

        -- Presist undos when the editor closes
        editor.persist_undos = or_default(editor.persist_undos, false)
        vim.opt.undofile = editor.persist_undos

        -- System clipboard
        editor.use_system_clipboard = or_default(editor.use_system_clipboard, false)
        if editor.use_system_clipboard then
            vim.cmd("set clipboard+=unnamedplus")
        else
            -- thanks ThePrimeagen <3
            vim.keymap.set({ "n", "v" }, "<leader>p", [["+p]])
            vim.keymap.set({ "n", "v" }, "<leader>P", [["+P]])
            vim.keymap.set({ "n", "v" }, "<leader>y", [["+y]])
            vim.keymap.set({ "n", "v" }, "<leader>Y", [["+Y]])
        end


        -- Setup GUI
        vim.cmd "au TextYankPost * silent! lua vim.highlight.on_yank()"
        vim.cmd "au TextYankPost * silent! lua vim.highlight.on_yank {higroup=\"FloatBorder\", timeout=200}"

        vim.cmd "set guifont=Noto\\ Sans\\ Meroitic:h11"
        if vim.g.neovide then
            vim.g.neovide_refresh_rate = 240
            vim.g.neovide_refresh_rate_idle = 60
            vim.g.neovide_cursor_vfx_mode = "wireframe"
            vim.g.neovide_cursor_animation_length = 0.05
            vim.g.neovide_cursor_trail_size = 0.65
            vim.g.neovide_cursor_antialiasing = true
            vim.g.neovide_confirm_quit = false
        end
    end
}
return M
