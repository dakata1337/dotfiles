local bootstrap = function()
    local lazypath = vim.fn.stdpath("data") .. "/lazy/lazy.nvim"
    if not vim.loop.fs_stat(lazypath) then
        vim.fn.system({
            "git",
            "clone",
            "--filter=blob:none",
            "https://github.com/folke/lazy.nvim.git",
            "--branch=stable", -- latest stable release
            lazypath,
        })
    end
    vim.opt.rtp:prepend(lazypath)
end

local M = {
    init = function(config)
        bootstrap()

        local internal_plugins = {
            -- Setup Which-Key
            {
                "folke/which-key.nvim",
                -- event = "VeryLazy",
                -- priority = 1000,
                config = function()
                    vim.o.timeout = true
                    vim.o.timeoutlen = 1500
                    require("which-key").setup()
                end
            },

            -- Telescope
            {
                "nvim-telescope/telescope.nvim",
                event = "VeryLazy",
                dependencies = {
                    "ThePrimeagen/harpoon",
                    "nvim-lua/plenary.nvim",
                },
                config = config.keymaps.telescope,
            },

            -- Highlighting
            {
                "nvim-treesitter/nvim-treesitter",
                event = "VeryLazy",
                config = function()
                    require("nvim-treesitter.configs").setup {
                        ensure_installed = { "rust", "lua" },
                        auto_install = true,
                        highlight = {
                            enable = true
                        }
                    }
                end
            },

            -- Autopairs
            {
                "windwp/nvim-autopairs",
                event = "VeryLazy",
                config = function()
                    require("nvim-autopairs").setup {
                        check_ts = true, -- Check Treesitter
                    }
                end
            },

            -- Comments
            {
                "folke/todo-comments.nvim",
                requires = "nvim-lua/plenary.nvim",
                event = "VeryLazy",
                config = function()
                    require("todo-comments").setup()
                    config.keymaps.todo_comments()
                end
            },
            {
                "numToStr/Comment.nvim",
                event = "VeryLazy",
                config = config.keymaps.comments
            },

            -- Git
            {
                "lewis6991/gitsigns.nvim",
                config = function()
                    require("gitsigns").setup()
                end,
            },

            -- UI
            { "stevearc/dressing.nvim",            event = "VeryLazy" },
            {
                "nvim-lualine/lualine.nvim",
                dependencies = {
                    "kyazdani42/nvim-web-devicons",
                },
                config = function()
                    require("statusline").config()
                end
            },

            -- LSP
            { "williamboman/mason.nvim",           event = "VeryLazy" },
            { "williamboman/mason-lspconfig.nvim", event = "VeryLazy" },
            { "neovim/nvim-lspconfig",             event = "VeryLazy" },
            { "hrsh7th/nvim-cmp",                  event = "VeryLazy" },
            { "hrsh7th/cmp-path",                  event = "VeryLazy" },
            { "hrsh7th/cmp-nvim-lsp",              event = "VeryLazy" },
            { "saadparwaiz1/cmp_luasnip",          event = "VeryLazy" },
            { "L3MON4D3/LuaSnip",                  event = "VeryLazy" },

            {
                "ThePrimeagen/harpoon",
                dependencies = {
                    "nvim-lua/plenary.nvim",
                },
                config = function()
                    config.keymaps.harpoon()
                end,
                event = "VeryLazy",
            },
        }

        for github_uri, value in pairs(config.plugins) do
            -- Lazy.nvim requires the first element to be
            -- a string representing the github url
            local plugin_builder = { github_uri }

            -- Move all configuration to `plugin_builder`
            for config_name, config_value in pairs(value) do
                plugin_builder[config_name] = config_value
            end

            -- Insert the user defined plugin to the table
            -- with the internal ones
            table.insert(internal_plugins, plugin_builder)
        end

        require("lazy").setup(internal_plugins, {
            dev = {
                path = "~/Programming/Lua",
            },
        })
    end
}
return M
